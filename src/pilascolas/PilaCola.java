/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pilascolas;

//import java.util.List;
//import java.util.Stack;
import java.util.*; //esto añade toda la libreria de Java.util, 
/**
 *
 * @author pcelardperez
 */
public class PilaCola {
    
    public PilaCola(){
        
    }
    
    //Push añade por el final LIFO (Last in first out).
    public void crearPila(Stack<Persoa> pila){
        pila.push(new Persoa("ana","11111"));
        pila.push(new Persoa("anton","2222"));
        pila.push(new Persoa("ccccc","33333"));
    }
    
    //Add tiene una estructura FIFO (First in first out).
    public void crearCola(List<Persoa> cola){
        cola.add(new Persoa("ana","11111"));
        cola.add(new Persoa("anton","2222"));
        cola.add(new Persoa("ccccc","33333"));
    }
    
    //mostramos la cola
    public void amosar(List<Persoa> cola){
        for(int i=0;i<cola.size();i++)
            System.out.println(cola.get(i));
        /*
        el get(i) devuelve todo el objeto por lo que hay que sobreescribirlo, para eso esta el metodo
        toString en la clase Persoa
         */
    }
}
