
package pilascolas;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/**
 *
 * @author pcelardperez
 */
public class Aplicacion {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        PilaCola obx = new PilaCola();
        
        /***Lista***/
        
        //Con este Stack creamos una pila vacía.
        Stack<Persoa> pila= new Stack<Persoa>();
        obx.crearPila(pila);
        System.out.println("Pila "+pila.peek());//no saca el elemento, si no que devuelve una copia del elemento sin sacarlo
                                                //el elemento sigue en la pila
        pila.pop();//saca o elemento da pila con .pop()
        System.out.println(pila.peek());
        //System.out.println("*** Colas ***");
        
        /***COLAS***/
        System.out.println("Cola");
        
        List<Persoa> cola = new LinkedList<Persoa>();
        obx.crearCola(cola); //crea la cola
        obx.amosar(cola); //muestra la cola por pantalla
            //sacar elemento de la cola, el primero que entró
        Persoa ob = cola.remove((0)); //saco da cola o primeiro elemento que entrou
        //recojo el objeto en ob para trabajar con el. Y a continuacion muestro la cola para comprobar de donde lo saque.
        obx.amosar(cola); //muestro la cola.
        
        
    }
    
}
