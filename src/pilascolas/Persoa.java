/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pilascolas;

/**
 *
 * @author pcelardperez
 */
public class Persoa {
    
    private String nome;
    private String dni;
    
    public Persoa(){
        
    }
    
    public Persoa(String nome, String dni){
        this.nome=nome;
        this.dni=dni;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }
    
    //Se crea este metodo para que no nos devuelva el Hash, y nos devuelve el String.
    public String toString(){
        return "El DNI de "+nome +" es "+ dni +" , ";
    }
    
}
